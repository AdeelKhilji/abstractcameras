/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalcameras;

/**
 * Class DigitalCameraSimulation
 * @author Adeel Khilji
 */
public class DigitalCameraSimulation 
{
    public static void main(String[] args)
    {
        System.out.println("DSLR:");//Displaying DSLR
        DigitalCamera pointAndShootCamera = new PointAndShootCamera("Canon","Powershot A590",8.0,0,16);//Creating a new PointAndShootCamera object
        System.out.println(pointAndShootCamera.describeCamera());//Displaying results
        System.out.println();//New Line
        System.out.println("PHONE:");//Displaying Phone
        DigitalCamera phoneCamera = new PhoneCamera("Apple", "iPhone", 6.0,64,0);//Creating a new PhoneCamera object
        System.out.println(phoneCamera.describeCamera());//Displaying results
    }
}
