/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalcameras;

/**
 * Class PointAndShootCamera extends DigitalCamera
 * @author Adeel Khilji
 */
public class PointAndShootCamera extends DigitalCamera
{
    /**
     * Constructor with five perimeters
     * @param make String
     * @param model String
     * @param megaPixels double
     * @param internalMemorySize int
     * @param externalMemorySize int
     */
    protected PointAndShootCamera(String make, String model, double megaPixels, int internalMemorySize, int externalMemorySize)
    {
        super(make,model,megaPixels,internalMemorySize,externalMemorySize);
    }
    
    /**
     * describeCamera - display method
     * @return String
     */
    public String describeCamera()
    {
        return "MAKE: " + super.getMake() + "\nMODEL: " + super.getModel() + "\nMEGA PIXELS: " + super.getMegaPixels() + "\nINTERNAL MEMORY SIZE: " + super.getInternalMemorySize() +"GB"+ "\nEXTERNAL MEMORY SIZE: " + super.getExternalMemorySize()+"GB";
    }
}
