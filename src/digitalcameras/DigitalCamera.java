/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalcameras;

/**
 * Abstract Class DigitalCamera
 * @author Adeel Khilji
 */
public abstract class DigitalCamera 
{
    private String make, model;//Declaring instance variables
    private double megaPixels;//Declaring instance variables
    private int internalMemorySize, externalMemorySize;//Declaring instance variables
    
    /**
     * Constructor with five perimeters
     * @param make String
     * @param model String
     * @param megaPixels double
     * @param internalMemorySize int
     * @param externalMemorySize int
     */
    protected DigitalCamera(String make, String model, double megaPixels, int internalMemorySize, int externalMemorySize)
    {
        this.make = make;
        this.model = model;
        this.megaPixels = megaPixels;
        this.internalMemorySize = internalMemorySize;
        this.externalMemorySize = externalMemorySize;
    }
    
    /**
     * getMake - getter method
     * @return String
     */
    public String getMake()
    {
        return this.make;
    }
    /**
     * getModel - getter method
     * @return String
     */
    public String getModel()
    {
        return this.model;
    }
    /**
     * getMegaPixels - getter method
     * @return double
     */
    public double getMegaPixels()
    {
        return this.megaPixels;
    }
    
    /**
     * getInternalMemorySize - getter method
     * @return int
     */
    public int getInternalMemorySize()
    {
        return this.internalMemorySize;
    }
    /**
     * getExternalMemorySize - getter method
     * @return int
     */
    public int getExternalMemorySize()
    {
        return this.externalMemorySize;
    }
    
    /**
     * describeCamera - display method
     * @return String
     */
    public String describeCamera()
    {
        return "MAKE: " + getMake() + "\nMODEL: " + getModel() + "\nMEGA PIXELS: " + getMegaPixels() + "\nINTERNAL MEMORY SIZE: " + getInternalMemorySize() +"GB"+ "\nEXTERNAL MEMORY SIZE: " + getExternalMemorySize() +"GB";
    }
}
